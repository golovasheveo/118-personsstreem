package telran.controller;

import telran.dto.Address;
import telran.dto.Child;
import telran.dto.Employee;
import telran.dto.Person;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomExportAppl {
    private static final long N_PERSONS = 1000;
    private static final int CHILD_MAX_YEAR = 2019;
    private static final int CHILD_MIN_YEAR = 2005;
    private static final int N_GARTENS = 3;
    private static final int MIN_EMPL_YEAR = 1983;
    private static final int MAX_EMPL_YEAR = 1993;
    private static final int N_COMPANIES = 3;
    private static final int MIN_SALARY = 5000;
    private static final int MAX_SALARY = 10000;
    private static final int N_CITIES = 3;
    private static final int PROBOBILITY = 30;
    private static final int MIN_ID = 100000000;
    private static final int MAX_ID = 199999999;
    private static final int N_NAMES = 100;
    private static final int N_BULDINGS = 100;
    private static final int N_APARTS = 300;
    private static final int N_STREETS = 100;


    public static void main(String[] args) throws Exception {
        List<Person> persons = getRandomPersons();
        try (ObjectOutputStream output =
                     new ObjectOutputStream(new FileOutputStream("persons.data"))) {
            output.writeObject(persons);
        }
    }

    private static List<Person> getRandomPersons() {
        return Stream.generate(RandomExportAppl::getRandomPerson)
                .limit(N_PERSONS)
                .collect(Collectors.toList());
    }

    private static Person getRandomPerson() {
        Person person = getRandomCommonPerson();
        return getChance() <= PROBOBILITY ? getRandomEmployee(person) : getRandomChild(person);
    }

    private static Person getRandomChild(Person person) {

        LocalDate birthDate = getRandomDate(CHILD_MIN_YEAR, CHILD_MAX_YEAR);
        String garten = "Garnet #" + getRandomNumber(1, N_GARTENS);

        return new Child(person.getId(), person.getName(),
                person.getAddress(), birthDate, garten);
    }

    private static int getRandomNumber(int min, int max) {
        Random random = new Random();
        return random
                .ints(1, min, max + 1)
                .findFirst()
                .getAsInt();
    }

    private static LocalDate getRandomDate(int minYear, int maxYear) {

        int year = getRandomNumber(minYear, maxYear);
        int month = getRandomNumber(1, 12);
        int dayOfMonth = getRandomNumber(1, 31);
        try {
            return LocalDate.of(year, month, dayOfMonth);
        } catch (Exception e) {
            return LocalDate.of(year, month, dayOfMonth - 10);
        }
    }

    private static Person getRandomEmployee(Person person) {
        String company = "Company #" + getRandomNumber(1, N_COMPANIES);
        int salary = getRandomNumber(MIN_SALARY, MAX_SALARY);
        String title = getRandomTitle();
        return new Employee(person.getId(), person.getName(),
                person.getAddress(), person.getBirthDate(),
                company, salary, title);
    }

    private static String getRandomTitle() {
        String[] titles = {"Manager", "QA", "DevOps", "Programmer"};
        return titles[getRandomNumber(0, titles.length - 1)];
    }

    private static int getChance() {

        return getRandomNumber(1, 100);
    }

    private static Person getRandomCommonPerson() {
        int id = getRandomNumber(MIN_ID, MAX_ID);
        LocalDate birthDate = getRandomDate(MIN_EMPL_YEAR, MAX_EMPL_YEAR);
        String name = "Name" + getRandomNumber(1, N_NAMES);
        Address address = getRandomAddress();
        return new Person(id, name, address, birthDate);
    }

    private static Address getRandomAddress() {

        String city = "City #" + getRandomNumber(1, N_CITIES);
        String street = "Street #" + getRandomNumber(1, N_STREETS);
        int building = getRandomNumber(1, N_BULDINGS);
        int aprt = getRandomNumber(1, N_APARTS);
        return new Address(city, street, building, aprt);
    }
}
