package telran.controller;

import telran.dto.Employee;
import telran.dto.Person;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ImportAppl {


    public static void main(String[] args) throws Exception {
        try (ObjectInputStream input = new ObjectInputStream
                (new FileInputStream("persons.data"))) {
            List<Person> persons = (List<Person>) input.readObject();

            displayMostPopulatedCities(persons);
            displayCompaniesAvgSalaries(persons);
            displayEmployeesSalary(persons);

        }

    }

    private static void displayEmployeesSalary(List<Person> persons) {
        System.out.println("\n Data about all employees with salary greater than overall average salary");
        double AvgSalary = getAvgSalary(persons);
        getStreamEmployees(persons).filter(e -> e.getSalary() > AvgSalary)
                .forEach(System.out::println);

    }

    private static double getAvgSalary(List<Person> persons) {
        return getStreamEmployees(persons).collect(Collectors.averagingInt(Employee::getSalary));
    }

    private static void displayCompaniesAvgSalaries(List<Person> persons) {
        System.out.println("\n Company names and averaging salary for each company");
        Map<String, Double> map =
                getStreamEmployees(persons)
                        .collect(Collectors.groupingBy(Employee::getCompany,
                                Collectors.averagingInt(Employee::getSalary)));
        map.forEach((k, v) -> System.out.printf("%s  %f\n", k, v));

    }

    private static void displayMostPopulatedCities(List<Person> persons) {
        System.out.println("\n Most populated cities");
        Map<String, Long> map = persons.stream()
                .collect(Collectors.groupingBy(p -> p.getAddress().getCity(),
                        Collectors.counting()));
        Long max = Collections.max(map.values());
        map.forEach((k, v) -> {
            if (v.equals(max)) {
                System.out.println(k);
            }
        });

    }

    static Stream<Employee> getStreamEmployees(List<Person> persons) {
        return persons.stream().filter(Employee.class::isInstance).map(Employee.class::cast);
    }

}
